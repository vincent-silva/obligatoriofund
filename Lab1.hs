{-#LANGUAGE GADTs, EmptyDataDecls, EmptyCase #-}
module Lab1 where
import Prelude (Show)

-- Ejercicio 1

id :: a -> a
id = \x -> x

-- Ejercicio 2
 
k :: a -> b -> a
k = \x -> \y -> x

kid :: a -> b -> b
kid = \x -> id

apply :: (a -> b) -> a -> b
apply = \x -> \y -> x y

flip :: (a -> b -> c) -> b -> a -> c
flip = \x -> \y -> \z -> x z y

-- Ejercicio 3

(.) :: (b ->c) -> (a->b) -> a -> c
(.) = \x -> \y -> \z -> x (y z)

-- Ejercicio 4

-- 3
res41 = id 3
-- 2
res42 = k 2 3
-- 3
res43 = kid 2 3
-- 3
res44 = (id.id) 3
-- 5
res45 = (id.kid) 4 5
-- 4
res46 = flip k 3 4

-- Ejercicio 5

f1 :: a -> (a -> b) -> b
f1 = \x -> \f -> f x

f2 :: (a -> a) -> a -> a
f2 = \f -> \x -> f (f (f x))

f3 :: a -> (a -> b) -> (b -> b -> c) -> c
f3 = \x -> \y -> \z -> z (y x) (y x)

f4 :: a -> b -> (b -> b -> c) -> c
f4 = \x -> \y -> \z-> z y y

f5 :: (a -> b -> c) -> (a -> a -> b) -> a -> c
f5 = \x -> \y -> \z -> x z (y z z)

-- Ejercicio 6

h1 :: (a -> b -> c) -> a -> b -> c
h1 = \x -> \y -> \z -> x y z

h2 :: (a -> b) -> (b -> c) -> a -> c
h2 = \x -> \y -> \z -> y (x z)

h3 :: (a -> b) -> (a -> b -> c) -> a -> c
h3 = \x -> \y -> \z -> y z (x z)

h4 :: (b -> c) -> (a -> c -> d) -> a -> b -> d
h4 = \x -> \y -> \z -> \v -> y z (x v)

h5a :: (a -> b -> a) -> b -> (b -> a) -> a
h5a = \x -> \y -> \z -> z y
-- segunda solucion
h5b :: (a -> b -> a) -> b -> (b -> a) -> a
h5b = \x -> \y -> \z -> x (z y) y


data Bool where {False::Bool ; True::Bool}
    deriving Show

--Ejercicio 7

not :: Bool -> Bool
not = \x-> case x of { False -> True ;
                       True -> False }
-- Son solo dos casos posibles
res71 = not True
res72 = not False

-- Ejercicio 8

-- True && True -> True
-- True && False -> True
-- False && True -> True
-- False && False -> False
(||) :: Bool -> Bool -> Bool
(||) = \x -> \y -> case x of { True -> True ; False -> y }

-- True && True -> True
-- True && False -> False
-- False && True -> False
-- False && False -> False
(&&) :: Bool -> Bool -> Bool
(&&) = \x -> \y -> case x of { False -> False ; True -> y }


-- True && True -> True
-- True && False -> False
-- False && True -> True
-- False && False -> True
(>>) :: Bool -> Bool -> Bool
(>>) = \x -> \y -> case x of { True -> y ; False -> True }

-- True && True -> False
-- True && False -> True
-- False && True -> True
-- False && False -> False
xor :: Bool -> Bool -> Bool
xor = \x -> \y -> case x of { True -> not y ; False -> y }

-- True && True -> False
-- True && False -> True
-- False && True -> False
-- False && False -> False
ni :: Bool -> Bool -> Bool
ni = \x -> \y -> case x of { True -> not y ; False -> False }

-- Ejercicio 9

-- (==) :: Bool -> Bool -> Bool
-- (==) = \x -> \y -> case x of {True -> y ; False -> not y}

eq :: Bool -> Bool -> Bool
eq = \x -> \y -> (x && y) || (not x && not y)

-- True && True -> False
-- True && False -> True
-- False && True -> True
-- False && False -> False
-- Son /= y XOR son equivalentes
-- (/=) :: Bool -> Bool -> Bool
-- (/=) = \x -> \y -> case x of {True -> not y ; False -> y}

-- Ejercicio 10
-- unanimidad ::  Bool -> Bool -> Bool -> Bool
-- unanimidad = \x -> \y -> \z -> (x == y) && (y== z)

mayoria :: Bool -> Bool -> Bool -> Bool
mayoria = \x -> \y -> \z -> x && (y || z) || (y && z)

ifthenelse ::  Bool -> a -> a -> a
ifthenelse = \x -> \y -> \z -> case x of {True -> y ; False -> z}