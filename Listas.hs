{-# LANGUAGE NPlusKPatterns #-}

module Listas where
    import Data.List hiding (null, length, sum, map, zip, zipWith, filter, and, or, any, all, (++), reverse, concat, head, tail, last,init,foldr,(!!))
    import Prelude(Show,Int,String,error)
    import Lab1
    import Lab2
    import Lab3

    null::[a]->Bool
    null = \a -> case a of {[] -> True ; x:xs -> False}

    length::[a]->N
    length = \l -> case l of {[] -> O ; [a] -> S O ; x:y:ys -> S (length (y:ys))}

    duplicate::[a]->[a]
    duplicate = \l -> case l of {[]->[] ; [a] -> [a,a] ; x:y:ys -> x:x: (duplicate (y:ys))}

    sum::[N]->N
    sum = \l -> case l of {[] -> O ; [a] -> a ; x:y:ys -> x + (sum (y:ys))}

    prod::[N]->N
    prod = \l -> case l of {[] -> error "Lista vacía" ; [a] -> a ; x:y:ys -> x * (prod (y:ys)) }

    map::(a->b) -> [a] -> [b]
    map = \f -> \l -> case l of {[] -> error "Lista vacía" ; [a] -> [f a] ; x:y:ys -> (f x) : (map f (y:ys))}

    zip::[a] -> [b] -> [(a,b)]
    zip = \l -> \j -> case l of {
        [] -> error "Lista vacía" ; 
        [a] -> case j of {[] -> [] ; y:ys -> [(a,y)]};
        x:z:zs -> case j of {[] -> [] ; y:ys -> (x,y):(zip (z:zs) (y:ys)) }
    }

    zipWith::(a->b->c)-> [a]-> [b]-> [c]
    zipWith = \f -> \l -> \j -> case l of {
        [] -> error "Lista vacía" ; 
        [a] -> case j of {[] -> [] ; y:ys -> [f a y]};
        x:z:zs -> case j of {[] -> [] ; y:ys -> (f x y):(zipWith f (z:zs) (y:ys)) }
    } 

    filter::(a->Bool)->[a]->[a]
    filter = \p -> \l -> case l of{
        [] -> error "Lista vacía" ; 
        [x] -> case p x of {True -> [x] ; false -> []} ;
        x:y:ys -> case p x of {True -> x:(filter p (y:ys)) ; False -> filter p (y:ys)}
    }

    and::[Bool]->Bool
    and = \l -> case l of {[] -> error "Lista vacía" ; [a] -> a ; x:y:ys -> x && (and (y:ys))}
    
    or::[Bool]->Bool
    or = \l -> case l of {[] -> error "Lista vacía" ; [a] -> a ; x:y:ys -> x || (and (y:ys))}

    any::(a->Bool)->[a]->Bool
    any = \p -> \l -> case l of {[] -> error "Lista vacía" ; [a] -> p a ; x:y:ys -> p x || (any p (y:ys))}

    all::(a->Bool)->[a]->Bool
    all = \p -> \l -> case l of {[] -> error "Lista vacía" ; [a] -> p a ; x:y:ys -> p x && (all p (y:ys))}

    (++)::[a]-> [a]-> [a]
    (++) = \l -> \j -> case l of {[] -> j ; x:xs -> x:(xs ++ j)}

    reverse::[a]->[a]
    reverse = \l -> case l of {[] -> [] ; x:xs -> (reverse xs) ++ [x]}

    --concat::[[a]] -> [a]
    --concat = \l -> case l of {[] -> [] ; [[a]] -> [a] ; x:xs -> (concat x):xs}

    pre:: Bool -> String -> a -> a
    pre = \b s x -> case b of { False -> error s  ;  True -> x }

    head1::[a]->a
    head1 = \l -> case l of {x:xs -> x}

    head::[a]->a
    head = \l -> pre ((length l) > O)
                "Lista vacía"
                (case l of {x:xs -> x})

    tail::[a]->[a]
    tail = \l -> pre (length l > O)
                "Lista vacía"
                (case l of {x:xs -> xs})

    last::[a]->a
    last = \l -> pre (length l > O)
                "Lista vacía"
                (case l of {[a] -> a ; x:xs -> last xs})

    init::[a]->[a]
    init = \l -> pre (length l > O)
                "Lista vacía"
                (case l of {[x] -> [] ; x:y:ys -> x:(init (y:ys))})

    (!!+)::[a]->N->a
    (!!+) = \l -> \n -> case l of {[] -> error "Indice largo"; x:xs -> case n of {O -> x; S k -> xs !!+ k}}

    (!!)::[a]->N->a
    (!!) = \l -> \n -> pre ((length l) > n) 
                    "Lista menor a posicion" 
                    (case n of {
                        O -> head l ; 
                        S k -> case l of {x:xs -> xs!!k}
                    })


    prefijo:: Eq a => [a] -> [a] -> Bool
    prefijo = \l -> \j -> case l of {[] -> True ; x:xs -> case j of{z:zs -> (x == z) && prefijo (xs) (zs)}}

    pares::[a]->[a]
    pares = \l -> case l of {[] -> [] ; [a] -> [a] ; x:z:zs -> x:(pares zs)}

    min::Ord a => [a] -> a
    min = \l -> case l of {[] -> error "Vacio" ; [x] -> x ; x:y:ys -> case x < min (y:ys) of {True -> x ; False -> min (y:ys)}}