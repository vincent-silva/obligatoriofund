{-# LANGUAGE NPlusKPatterns #-}

module Sorting where
    -- import Data.List hiding (null, length, sum, map, zip, zipWith, filter, and, or, any, all, (++), reverse, concat, head, tail, last,init,foldr,(!!))
    -- import Prelude(Show,Int,String,error)
    -- import Lab1
    -- import Lab2
    -- import Lab3
    -- import Listas

    import Prelude


    -- insert :: Ord a => a -> [a] -> [a]
    -- insert = \x l -> case l of {[] -> [x]; z:zs -> case x<z of {False -> z:insert x zs; True -> x:z:zs}}
    -- rec. primitiva en 1

    -- insertSort :: Ord a -> a => [a] -> [a]
    -- isnertSord = \l -< case l of {[] -> []; z:zs -> insert z (insertSort zs)}
    -- rec. primitiva en 1


    -- Select sort: seleccionar el minimo elemento de la lista y ponerlo adelante del resto de la lista ordenado

    --- SELECT SORT ---
    -- minL :: Ord a => [a] -> a
    -- minL = \l -> case l of {[] -> error "Lista vacía" ;[x] -> x; z:y:ys -> min z (minL (y:ys))}
    -- rec. estructural. EX-EX. Llam rec. contenida
    -- estructuralmente mas chicas porque x:y:ys > y:ys

    -- selectSort :: [a] -> [a]
    -- selectSort = \l -> case l of {[] -> [] ; z:zs -> (minL l):selectSort (borrar1 (minL l) l)}
    -- rec. bien fundada, ya que el compilador no puede verificar que la recursión es finita.
    -- rec. bien fundada: La llamada recursiva se hace sobre una lista que tiene un elemento menos que la lista original


    -- Merge sort, (ordenación por intercalamiento): Se divide la lista en dos y se ordenan ambas listas utilizando el mismo método (recursivamente) y
    -- se intercalan los elementos de las listas ordenadas

    -- split:: [a] -> ([a],[a])
    -- split = \l -> case l of {
        -- [] -> ([],[]) ; 
        -- [x] -> ([x],[]) ;
        -- x:y:ys -> (x:first (split ys),y:snd (split ys))
    -- }
    -- rec. estructural

    -- merge::Ord a => [a] -> [a] -> [a]
    -- merge = \l -> \j -> case l of {
        -- [] -> j;
        -- x:xs -> case j of {
            -- [] -> l;
            -- y:ys -> case x < y of {
                -- True    -> x:merge xs j ; 
                -- False   -> y:merge l ys
            -- }
        -- }
    -- }

    -- rec. primitiva anidada

    {-mergeSort:: Ord a => [a] -> [a]
    mergeSort = \l -> case l of {
        [] -> [];
        [a] -> [a];
        x:z:zs -> case split l of {(xs,ys) -> merge (mergeSort xs) (mergeSort ys)}
    }-}

    -- rec. bien fundada en el caso x:z:zs, el split me da dos listas mas chicas
    -- que la lista original (Tuvimos que agregar 2 casos base para asegurarons de que esta condicion se cumple)


    -- QUICK SORT
    quickSort::Ord a => [a] -> [a]
    quickSort = \l -> case l of {
        [] -> [];
        z:zs -> quickSort (filter (<z) zs) ++ z : quickSort(filter (>=z) zs)
    }

    -- rec. bien fundada: las llamadas recursivas se hacen sobre filter de cierta condicion de zs
    -- que tiene menos elementos que z:zs