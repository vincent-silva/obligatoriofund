{-# OPTIONS_GHC -fno-warn-tabs #-}
{-#LANGUAGE GADTs, EmptyDataDecls, EmptyCase #-}

module Arboles where
    data BT where {
        O :: BT;
        I :: BT;
        P :: [BT] -> BT;
        S :: [BT] -> BT 
    } deriving Show

    arbol = P [S [I,O,P [O,I]],I]

{-    unos :: BT -> Int
    unos = \t -> case t of {
        I -> True;
        O -> False;
        P ts -> sum (map valor ts);
        S ts -> sum (map valor ts)
    }
-}
    valor :: BT -> Bool
    valor = \t -> case t of {
        I -> True;
        O -> False;
        P ts -> and (map valor ts);
        S ts -> or (map valor ts)
    }

    data LArbol a where {Nodo :: a -> [LArbol a] -> LArbol a} deriving Show

    larbol = Nodo 3 [Nodo 2 [Nodo 1 [],Nodo 5 []],Nodo 6 [],Nodo 7 [Nodo 8 []]]

    cantNodosLA::LArbol a -> Int
    cantNodosLA = \t -> case t of {Nodo v ts -> 1 + sum (map cantNodosLA ts)} 

    alturaLa::LArbol a -> Int
    alturaLa = \t -> case t of {
        Nodo v [] -> 1;
        Nodo v ts -> 1 + maximum (map alturaLa ts)
    }

    mapLA::(a->b) -> LArbol a -> LArbol b
    mapLA = \f -> \t -> case t of {
        Nodo v ts -> Nodo (f v) (map (mapLA f) ts)
    }

    aridad::LArbol a -> Int
    aridad = \t -> case t of {
        Nodo v ts -> maximum((length ts):(map aridad ts))
    }

    larbol2list::LArbol a -> [a]
    larbol2list = \t -> case t of {
        Nodo v ts -> v:concat (map (larbol2list ) ts)
    }

{-    larbol2list2::LArbol a -> [a]
    larbol2list2 = \t -> case t of {
        Nodo v ts -> case ts of {
            [] -> [x];
            x:xs -> undefined {- (larbol2list2 x [])++larbol2list2(N v xs)-}
        }
    }-}


    data L a b where {
        V :: L a b;
        NA :: a -> L a b -> L a b;
        NB :: b -> L a b -> L a b
    } deriving Show    

    data A where {
        H :: A;
        U :: A -> A;
        T :: A -> A -> A -> A
    } deriving Show

    arbolA = T H H (U (T H H H))

    nodos::A->Int
    nodos = \t -> case t of {
        H -> 1;
        U x -> 1 + nodos x;
        T t1 t2 t3 -> 1 + (nodos t1) + (nodos t2) + (nodos t3) 
    }

    unos :: A -> Int
    unos = \t -> case t of {
        H -> 0;
        U t -> 1 + unos t;
        T t1 t2 t3 -> (unos t1) + (unos t2) + (unos t3) 
    }

    trans :: A -> A
    trans = \t -> case t of {
        H -> H;
        U t -> T H (trans t) H;
        T t1 t2 t3 -> T (trans t1) (trans t2) (trans t3)
    }

