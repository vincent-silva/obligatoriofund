{-#LANGUAGE GADTs, EmptyDataDecls, EmptyCase #-}

module Lab3 where
    import Prelude (Show)
    import Lab1
    import Lab2

    data N where {O::N ; S::N->N}
        deriving Show

    pos::N -> Bool
    pos = \n-> case n of {O->False ; S x -> True}

    uno::N
    uno = S O

    dos::N
    dos = S uno

    tres::N
    tres = S dos

    -- Ejercicio 2

    pred::N->N
    pred = \n -> case n of {O -> O ; S x -> x}


    par::N -> Bool
    par = \n -> case n of {O -> True ; S x -> not (par x)}

    impar::N -> Bool
    impar = \n -> case n of {O -> False ; S x -> not (impar x)}

    impar2::N -> Bool
    impar2 = \n -> not (par n)

    doble::N -> N
    doble = \n -> case n  of {O -> O ; S x -> S(S( doble x ))}

    triple::N -> N
    triple = \n -> case n of {O -> O ; S x -> S(S(S( triple x )))}

    existe::N->(N->Bool)->Bool
    existe = \n -> \p -> case n of {O -> p O ; S x -> (p (S x)) || (existe x p)}

    todos::N->(N->Bool)->Bool 
    todos = \n -> \p -> case n of {O -> p O ; S x -> (p (S x)) && (todos x p)}

    contar::N->(N->Bool)->N
    contar = \n -> \p -> case n of {
        O -> O ;
        S x -> case p (S x) of {True -> S(contar x p) ; False -> contar x p}
    }

    (+)::N->N->N
    (+) = \a -> \b -> case a of {O -> b ; S x -> S(x+b) }

    (*)::N->N->N
    (*) = \a -> \b -> case a of {O -> O ; S x -> b + (x * b)}

    (^)::N->N->N
    (^)= \a -> \b -> case b of {O -> S O ; S x -> a * (a ^ x)} 

    fact::N->N
    fact = \a -> case a of {O -> S O; S x -> a * (fact x)}

    sumi::N->N
    sumi = \a -> case a of {O -> O ; S x -> a + (fact x)}
    
    sumidobles::N->N
    sumidobles = \a -> case a of {O->O ; S x -> (doble a) + (sumidobles x)}

    sumfi::(N->N)->N->N
    sumfi = \f -> \a -> case a of {O->f O ; S x -> (f a) + (sumfi f x)}

    sumpares::N->N
    sumpares = \a -> case a of {O->O ; 
        S x -> case par a of {True -> a ; False -> O} + (sumpares x)
    }

    sumpi::(N->Bool)->N->N
    sumpi = \p -> \a -> case a of {
        O -> O ; 
        S x -> case p a of {True -> a ; False -> O} + sumpi p x
    }

    instance Eq N where
        (==) = \a -> \b -> case a of {
            O -> case b of {O -> True ; S y -> False};
            S x -> case b of {O -> False ; S y -> x == y}
        }

    instance Ord N where
        (<=) = \a -> \b -> case a of {
            O -> True ;
            S x -> case b of {O -> False ; S y -> x <= y}
        }