{-# OPTIONS_GHC -fno-warn-tabs #-}
module Datos2 where

-- Ejemplo de atributos sacados de fotos de superheroes de Batman

data Atributo = Hombre | Mascara | Capa | Corbata | Orejas | Fumador
	deriving ( Eq , Ord , Enum , Show )
		
data Clase = Bueno | Malo
	deriving ( Eq , Ord , Enum , Show )
	
type Nombre = String
	
type Registro = (Nombre,Clase,[Bool])


ejs :: [Registro]
ejs = [ ("batman",Bueno,[True,True,True,False,True,False]),
		("batichica",Bueno,[False,True,True,False,True,False]),
		("robin",Bueno,[True,True,True,False,False,False]),
		("alfred",Bueno,[True,False,False,True,False,False]),
		("pinguino",Malo,[True,False,False,True,False,True]),
		("gatubela",Malo,[False,True,False,False,True,False]),
		("guason",Malo,[True,False,False,False,False,False])]

-- arDec = Nodo Capa (Nodo Hombre (Hoja Malo) (Nodo Mascara (Nodo Orejas (Nodo Corbata (Hoja Malo) (Nodo Fumador (Hoja Bueno) (Hoja Malo))) (Hoja Malo)) (Hoja Malo))) (Hoja Bueno)


tests :: [(Nombre,[Bool])]
tests = [("acertijo",[True,True,False,False,False,False]),
		 ("mujermaravilla",[False,False,True,False,True,False])]

-- clasificacion = [("acertijo",Malo),("mujermaravilla",Bueno)]
		 