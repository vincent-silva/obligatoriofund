{-# OPTIONS_GHC -fno-warn-tabs #-}
module Datos3 where
-- Ejemplo de atributos para decidir si jugamos al Golf segun las condiciones ambientales

data Atributo = Humedad | Frio | SolFuerte | Viento 
	deriving (Eq,Ord,Enum,Show)
		
data Clase = Juego | NoJuego
	deriving  (Eq,Ord,Enum,Show)
	
type Nombre = String
	
type Registro = (Nombre,Clase,[Bool])
type Datos = [Registro]

ejs :: Datos
ejs = [ ("lunes1",NoJuego,[True,False,True,False]),
		("martes2",NoJuego,[True,False,True,True]),
		("miercoles3",Juego,[True,False,True,False]),
		("jueves4",Juego,[False,True,True,False]),
		("viernes5",Juego,[False,True,False,False]),
		("sabado6",NoJuego,[False,True,False,True]),
		("domingo7",Juego,[True,True,False,True]),
		("lunes8",NoJuego,[True,True,True,False]),
		("martes9",Juego,[True,True,False,False]),
		("miercoles10",Juego,[False,True,False,False]),
		("jueves11",Juego,[True,True,False,True]),
		("viernes12",NoJuego,[True,True,True,True]),
		("sabado13",Juego,[True,False,False,False]),
		("domingo14",NoJuego,[False,True,True,True])]

-- arDec = Nodo Frio (Nodo Humedad (Hoja Juego) (Nodo SolFuerte (Hoja Juego) (Nodo Viento (Hoja Juego) (Hoja NoJuego)))) (Nodo SolFuerte (Nodo Humedad (Nodo Viento (Hoja Juego) (Hoja NoJuego)) (Hoja Juego)) (Nodo Humedad (Nodo Viento (Hoja Juego) (Hoja NoJuego)) (Hoja NoJuego)))

		
	
tests :: [(Nombre,[Bool])]
tests = [("lunes15",[False,True,True,False]),
		("martes16",[True,True,True,False])]
		
-- clasificacion = [("lunes15",Juego),("martes16",NoJuego)]