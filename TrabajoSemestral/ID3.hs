{-# OPTIONS_GHC -fno-warn-tabs #-}
{-#LANGUAGE GADTs, EmptyDataDecls, EmptyCase #-}

-- Vincent Silva 212677
module ID3 where
import Datos
import Auxiliares
    
data ArBin a b where {
    Hoja :: b -> ArBin a b ; 
    Nodo :: a -> (ArBin a b) -> (ArBin a b) -> (ArBin a b) 
}
  deriving Show
    
type ArDec = ArBin Atributo Clase

--12
-- Construye el arbol de decision
id3 :: [Registro] -> [Atributo] -> Clase -> ArDec
id3 = \rgs -> \ats -> \c -> case rgs of {
    [] -> Hoja c;
    (w,y,z):xs -> case length(rgs) == length(regsClase y rgs) of {
        True -> Hoja y;
        False -> case ats of {
            [] -> Hoja (maxClase rgs);
            z:zs -> case minAtr ats rgs of {
                a -> Nodo a 
                    (id3 (regsAtrB a False rgs) (filter (/= a) ats) (maxClase rgs))
                    (id3 (regsAtrB a True rgs) (filter (/= a) ats) (maxClase rgs))
            }
        }
    }
}

--13
-- Pasa los parametros a la funcion id3 que construye el arbol de decision
arDec :: ArDec
arDec = id3 ejs atributos (maxClase ejs)

--14
-- Clasifica usando un arbol, una tupla de nombre y lista de atributos en un nombre y clase
clasificar :: ArDec -> (Nombre,[Bool]) -> (Nombre,Clase)
clasificar = \ard -> \ats -> case ard of {
    Hoja x -> (fst(ats),x);
    Nodo x y z -> case snd(ats)!!(fromEnum x) of {
        True -> clasificar z ats;
        False -> clasificar y ats
    }
}

-- Retorna la clasificación de una lista de casos de prueba
clasificacion :: [(Nombre,Clase)]
clasificacion = map (clasificar arDec) tests


