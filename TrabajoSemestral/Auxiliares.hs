{-# OPTIONS_GHC -fno-warn-tabs #-}

-- Vincent Silva 212677
module Auxiliares where
import Datos
    
--1
atributos :: [Atributo]
atributos = [toEnum 0 ..]

clases :: [Clase]
clases = [toEnum 0 ..]

--2
-- Devuelve la clase para un nombre en una lista de registros
clase :: Nombre -> [Registro] -> Clase
clase = \n -> \rgs -> case rgs of {
    [] -> error "Lista de registros vacía" ; 
    (a,b,c):xs ->  case a == n of {
        True -> b; 
        False -> clase n xs
    }
}

--3
-- Devuelve el valor de un atributo en un registro
valorAtr :: Atributo -> Registro -> Bool
valorAtr = \a (n,c,bs) -> bs!!(fromEnum a) 

--4
-- Devuelve la lista de registros para los cuales el atributo tiene un valor
regsAtrB :: Atributo -> Bool -> [Registro] -> [Registro]
regsAtrB = \a v r -> case r of {
    [] -> [] ; 
    x:xs -> case (valorAtr a x) == v of {
        True -> x:(regsAtrB a v xs);
        False -> regsAtrB a v xs
    }
}

--5 
-- Devuelve la lista de registros cuyos elementos pertenecen a una clase
regsClase :: Clase -> [Registro] -> [Registro]
regsClase = \c r -> case r of {
    [] -> [];
    (w,y,z):xs -> case y == c of {
        True -> (w,y,z):(regsClase c xs); 
        False -> regsClase c xs
    }  
}

--6 
-- Calcula la proporcion entre los registros que tienen una clase y un atributo es verdadero, dividido todos los registros
prop :: Clase -> Atributo -> [Registro] -> Float
prop = \c a rgs -> case rgs of {
    [] -> 0;
    x:xs -> case regsClase c rgs of{
        [] -> 0;
        x:xs -> fromIntegral (length (regsAtrB a True (regsClase c rgs))) / fromIntegral (length (regsClase c rgs))
    } 
} 

--7
-- Calcula la entropia de un atributo para una clase determinada
entrAtrClase :: Clase -> Atributo -> [Registro] -> Float
entrAtrClase = \c a rs -> case prop c a rs of {
    0 -> 0;
    x -> (x * log(x))
}

--8
-- Calcula la sumatoria de la entropía de un atributo para una lista de clases
entrAtrClases :: [Clase] -> Atributo -> [Registro] -> Float
entrAtrClases = \cs a rgs -> case cs of {
    [] -> 0; 
    x:xs -> (entrAtrClases xs a rgs) + (entrAtrClase x a rgs)
}

-- Devuelve la entropia de un atributo si pertenece a una lista de atributos
entrAtr :: [Atributo] -> [Registro] -> Atributo -> Float
entrAtr = \ats rgs a -> case elem a ats of {
    False -> fromIntegral 1;
    True -> -1 * (entrAtrClases clases a rgs)
}

--9
-- Devuelve el atributo de menor entropía en una lista de registros
minAtr :: [Atributo] -> [Registro] -> Atributo
minAtr = \ats -> \rgs -> case ats of {
    [] -> error "Lista vacía";
    [x] -> x;
    x:xs -> case (entrAtr ats rgs x) <= (entrAtr ats rgs (minAtr xs rgs)) of {
        True -> x;
        False -> minAtr xs rgs
    }
}

--10                
-- Divide una lista en dos para los cuales el atributo es falso y verdadero
partAtr :: Atributo -> [Registro] -> ([Registro],[Registro])
partAtr = \a -> \rgs -> ((regsAtrB a False rgs), (regsAtrB a True rgs))

--11
-- Devuelve la clase mas representativa de la lista de registros
maxClase :: [Registro] -> Clase
maxClase = \rgs -> fst (maxElem (ocurrencias rgs))


-- Devuelve la tupla que tiene el valor maximo
maxElem :: [(Clase, Int)] -> (Clase, Int)
maxElem = \oc -> case oc of{
    [] -> error "Lista vacía";
    [x] -> x;
    x:y:ys -> case snd(x) > snd(maxElem (y:ys)) of {
        True -> x;
        False -> maxElem (y:ys)
    }
}

-- Devuelve la lista de ocurrencias para cada clase
ocurrencias :: [Registro] -> [(Clase,Int)]
ocurrencias = \rgs ->  case rgs of {
    [] -> [];
    (w,y,z):xs -> 
        (y, length(regsClase y rgs)):
        (ocurrencias (filter (\(a,b,c) -> (b /= y)) xs))
}




