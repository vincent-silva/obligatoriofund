{-#LANGUAGE GADTs, EmptyDataDecls, EmptyCase #-}

-- Vincent Silva 212677
module Entregable3 where

    type Conj a = [a]

    prim :: Conj Int
    prim = [2,3,5,7,11]

    pert :: Eq a => a -> Conj a -> Bool
    pert = \a -> \c -> case c of {[] -> False ; x:xs -> (a == x) || (pert a xs)}

    esConj :: Eq a => [a] -> Bool
    esConj = \l -> case l of {[] -> True ; x:xs -> (not (pert x xs)) && esConj xs}

    incl :: Eq a => Conj a -> Conj a -> Bool
    incl = \a -> \b -> case a of {[] -> True ; x:xs -> (pert x b) && (incl xs b)}

    inter :: Eq a => Conj a -> Conj a -> Conj a
    inter = \a -> \b -> case a of {
        [] -> [] ; 
        x:xs -> case pert x b of {True -> x:(inter xs b) ; False -> inter xs b} 
    }