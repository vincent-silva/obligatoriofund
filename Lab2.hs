{-#LANGUAGE GADTs, EmptyDataDecls, EmptyCase #-}

module Lab2 where
    import Prelude (Show)
    import Lab1

    data Carga where {Positivo::Carga; Negativo::Carga} deriving Show

    -- Ejercicio 1
    opuesto::Carga -> Carga
    opuesto = \x -> case x of {Negativo -> Positivo ; Positivo->Negativo}

    -- Ejercicio 2
    data Dia where {Lu::Dia;Ma::Dia;Mi::Dia;Ju::Dia;Vi::Dia;Sa::Dia;Do::Dia} deriving Show

    siguiente::Dia -> Dia
    siguiente = \x -> case x of {Lu -> Ma ; Ma -> Mi ; Mi -> Ju ; Ju -> Vi ; Vi -> Sa ; Sa -> Do ; Do -> Lu}

    laborable::Dia -> Bool
    laborable = \x -> case x of {Lu -> True ; Ma -> True ; Mi -> True ; Ju -> True ; Vi -> True ; Sa -> False ; Do -> False}


    -- Ejercicio 3

    data PPT where {Piedra::PPT ; Papel::PPT ; Tijera::PPT}

    gana::PPT -> PPT -> PPT
    gana = \x -> \y -> case x of {
        Piedra -> case y of {Piedra -> Piedra ; Papel -> Papel ; Tijera -> Piedra};
        Papel -> case y of {Piedra -> Papel ; Papel -> Papel ; Tijera -> Tijera};
        Tijera -> case y of {Piedra -> Piedra ; Papel -> Tijera ; Tijera -> Tijera};
    } 

    -- Ejercicio 4

    class Eq a where {
        (==),(/=) :: a -> a -> Bool;
        (/=) = \x y -> not (x == y)
    }

    class Eq a => Ord a where {
        (<=), (<), (>=), (>) :: a -> a -> Bool;
        (>) = \x y -> not (x <= y);
        (>=) = \x y -> (x > y) || (x == y);
        (<) = flip (>)
    }

    -- Ejercicio 5

    instance Eq Bool where
        (==) = \x -> \y -> case x of {True -> y; False -> not y}

    instance Ord Bool where
        (<=) = \x -> \y -> case x of {True -> y; False -> True}

    -- Ejercicio 6

    instance Eq PPT where {
        (==) = \x -> \y -> case x of {
            Piedra -> case y of {Piedra -> True ; Papel -> False ; Tijera -> False};
            Papel -> case y of {Piedra -> False ; Papel -> True ; Tijera -> False};
            Tijera -> case y of {Piedra -> False ; Papel -> False ; Tijera -> True}
        }
    }

    instance Ord PPT where {
        (<=) = \x -> \y -> case x of {
            Piedra -> case y of {Piedra -> True ; Papel -> True ; Tijera -> False};
            Papel -> case y of {Piedra -> False ; Papel -> True ; Tijera -> True};
            Tijera -> case y of {Piedra -> True ; Papel -> False ; Tijera -> True};
        }
    }

    
    -- Ejercicio 7

    minimo :: Ord a => a -> a -> a
    minimo = \x -> \y -> case x < y of {True -> x; False -> y}

    maximo :: Ord a => a -> a -> a
    maximo = \x -> \y -> case x > y of {True -> x; False -> y}

    -- Ejercicio 8

    min3 :: Ord a => a -> a -> a -> a
    min3 = \x -> \y -> \z -> minimo (minimo x y) z